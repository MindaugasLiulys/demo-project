### Laravel 6.15.0 + VueJs 2.5.17

## Installation

Clone this repository to your computer.  
After that, go to your project root directory and install composer.

```
composer install
```

Create a new database for this project.  

Copy .env file from .env.example.  

```
cp .env.example .env
```

Change these fields according to your new database

```
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
Generate APP_KEY

```
php artisan key:generate
```
Install npm

```
npm install
```

Generate JWT_SECRET key

```
php artisan jwt:secret
```

Run migrations and seeder

```
php artisan migrate --seed
```

### Set correct permissions to /bootstrap and /storage directories


Build npm for development

```
npm run dev
```

or for production

```
npm run prod
```

Test user logins

```
admin@admin.com
secret
```