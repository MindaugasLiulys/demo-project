import ProductList from '@/Products/ProductList'
import ProductShow from '@/Products/ProductShow'

export const productRoutes = [
  {
    path: '/products',
    name: 'products',
    component: ProductList,
  },
  {
    path: '/products/:id',
    name: 'product',
    component: ProductShow,
  },
]