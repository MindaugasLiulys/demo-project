import Login from '@/Auth/Login'
import Register from '@/Auth/Register'

export const authRoutes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
  }
]