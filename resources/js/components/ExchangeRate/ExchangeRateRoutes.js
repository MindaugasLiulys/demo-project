import ExchangeRateList from '@/ExchangeRate/ExchangeRateList'

export const exchangeRateRoutes = [
  {
    path: '/exchange-rate',
    name: 'exchangeRate',
    component: ExchangeRateList,
  }
]