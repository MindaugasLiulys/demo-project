import ArticleList from '@/Articles/ArticleList'
import ArticleShow from '@/Articles/ArticleShow'
import AdminArticleList from '@/Articles/Admin/AdminArticleList'
import AdminArticleCreate from '@/Articles/Admin/AdminArticleCreate'
import AdminArticleEdit from '@/Articles/Admin/AdminArticleEdit'
import auth from '*/middleware/auth'

export const articleRoutes = [
  {
    path: '/articles',
    name: 'articles',
    component: ArticleList,
  },
  {
    path: '/articles/:slug',
    name: 'article',
    component: ArticleShow,
  },
  {
    path: '/admin/articles',
    name: 'adminArticles',
    component: AdminArticleList,
    meta: {
      auth: true,
      middleware: [auth]
    }
  },
  {
    path: '/admin/articles/create',
    name: 'adminArticlesCreate',
    component: AdminArticleCreate,
    meta: {
      auth: true,
      middleware: [auth]
    }
  },
  {
    path: '/admin/articles/edit/:id',
    name: 'adminArticlesEdit',
    component: AdminArticleEdit,
    meta: {
      auth: true,
      middleware: [auth]
    }
  },
]