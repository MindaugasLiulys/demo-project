import AuthService from '*/services/AuthService'
import ProductService from '*/services/ProductService'
import CategoriesService from '*/services/CategoriesService'
import VariationsService from '*/services/VariationsService'
import ArticlesService from '*/services/ArticlesService'
import ExchangeRateService from '*/services/ExchangeRateService'
import CommentsService from '*/services/CommentsService'

export const $api = {
  auth: AuthService,
  products: ProductService,
  categories: CategoriesService,
  variations: VariationsService,
  articles: ArticlesService,
  exchangeRate: ExchangeRateService,
  comments: CommentsService
}

const ApiPlugin = {
  install (Vue) {
    Vue.prototype.$api = $api
  }
}

export default ApiPlugin
