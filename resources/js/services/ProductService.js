import Vue from 'vue'

export default {
  list(params) {
    return Vue.axios.get(`/products`, { params })
  },

  show(product) {
    return Vue.axios.get(`/products/${product}`)
  }
}
