import Vue from 'vue'

export default {
  getAvailableCurrencies() {
    return Vue.axios.get(`/exchange-rate/available-currencies`)
  },

  getMultipleRates(currencies) {
    return Vue.axios.get(`/exchange-rate`, { params: {currencies: currencies} })
  }
}
