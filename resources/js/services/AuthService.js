import Vue from 'vue'

export default {
  register (data) {
    return Vue.axios.post('/auth/register', data)
  },

  login (data) {
    return Vue.axios.post('/auth/login', data)
  }
}
