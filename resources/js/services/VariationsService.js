import Vue from 'vue'

export default {
  list() {
    return Vue.axios.get(`/variations`)
  }
}
