import Vue from 'vue'

export default {
  list(modelId, modelType, params) {
    return Vue.axios.get(`/comments/get-model-comments/${modelId}/${modelType}`, { params })
  },

  store(data) {
    return Vue.axios.post(`/comments`, data)
  },

  admin: {
  //   update(id, data) {
  //     return Vue.axios.post(`/admin/articles/update/${id}`, data)
  //   },

    delete(id) {
      return Vue.axios.delete(`/admin/comments/${id}`)
    }
  }
}
