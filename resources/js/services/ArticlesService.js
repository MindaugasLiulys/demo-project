import Vue from 'vue'

export default {
  list(params) {
    return Vue.axios.get(`/articles`, { params })
  },

  show(id) {
    return Vue.axios.get(`/articles/${id}`)
  },

  admin: {
    list(params) {
      return Vue.axios.get(`/admin/articles`, { params })
    },

    show(id) {
      return Vue.axios.get(`/admin/articles/${id}`)
    },

    create(data) {
      return Vue.axios.post(`/admin/articles`, data)
    },

    update(id, data) {
      return Vue.axios.post(`/admin/articles/update/${id}`, data)
    },

    delete(id) {
      return Vue.axios.delete(`/admin/articles/${id}`)
    }
  }
}
