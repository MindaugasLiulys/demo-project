import _ from 'lodash'

export default class Comment {
  id = null
  username = null
  text = null
  model_id = null
  model_type = null
  createdAt = null

  constructor (data) {
    _.forEach(data, (value, field) => {
        this[field] = value
    })
  }
}