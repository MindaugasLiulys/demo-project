import _ from 'lodash'

export default class User {
  id = null
  name = null
  email = null
  isAdmin = false

  constructor (data) {
    _.forEach(data, (value, field) => {
      if (this.hasOwnProperty(field)) {
        this[field] = value
      }
    })
  }
}