import _ from 'lodash'

export default class Product {
  id = null
  title = null
  description = null
  price = null
  images = []
  categories = []
  variations = []

  constructor (data) {
    _.forEach(data, (value, field) => {
        this[field] = value
    })
  }
}