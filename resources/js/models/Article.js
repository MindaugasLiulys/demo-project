import _ from 'lodash'
import User from '*/models/User'

export default class Article {
  id = null
  title = null
  slug = null
  text = null
  status = null
  statusText = null
  author = {}
  attachments = []
  comments = []
  modelType = null
  createdAt = null

  constructor (data) {
    _.forEach(data, (value, field) => {
        this[field] = value
    })

    this.author = new User(data.author || {})
  }
}