import _ from 'lodash'

export default class ProductImage {
  id = null
  path = null

  constructor (data) {
    _.forEach(data, (value, field) => {
        this[field] = value
    })
  }
}