import _ from 'lodash'

export default class Variation {
  id = null
  title = null
  value = null
  notes = null
  products = []

  constructor (data) {
    _.forEach(data, (value, field) => {
        this[field] = value
    })
  }
}