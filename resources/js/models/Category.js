import _ from 'lodash'
import ProductImage from '*/models/ProductImage'

export default class Category {
  id = null
  title = null
  products = []

  constructor (data) {
    _.forEach(data, (value, field) => {
        this[field] = value
    })
  }
}