import Toastr from 'toastr'

const ToastrPlugin = {
  install (Vue) {
    Vue.prototype.$toastr = Toastr
  }
}

export default ToastrPlugin