import Vue from 'vue'
import router from './router'
import AppComponent from '@/App'
import VueAxios from 'vue-axios'
import axios from 'axios'
import ApiPlugin from '*/services/api'
import ToastrPlugin from '*/plugins/toastr'
import VueAuth from '@websanova/vue-auth'
import User from '*/models/User'
import GlobalPlugin from '*/global'
import NavigationBack from '@/Layout/NavigationBack'
import FileUpload from '*/plugins/FileUpload'

require('bootstrap');

Vue.use(VueAxios, axios)
Vue.use(ApiPlugin)
Vue.use(ToastrPlugin)
Vue.use(GlobalPlugin)

Vue.axios.defaults.baseURL = '/api'

Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('navigationBack', NavigationBack);
Vue.component('FileUpload', FileUpload);

Vue.router = router

Vue.use(VueAuth, {
  auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  parseUserData: function (data) {
    return new User(data.data)
  }
});

new Vue({
  el: '#app',
  render: (h) => h(AppComponent),
  router,
});
