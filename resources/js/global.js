import _ from 'lodash'

export const $global = {
  parseJsonToString(JsonObject, itemKey) {
    let item = []

    _.each(JsonObject, (key, value) => {
      item.push(eval('key.' + itemKey))
    })

    return item.join(', ')
  }
}

const GlobalPlugin = {
  install (Vue) {
    Vue.prototype.$global = $global
  }
}

export default GlobalPlugin
