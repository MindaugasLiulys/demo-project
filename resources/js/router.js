import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/Home'
import { authRoutes } from '@/Auth/AuthRoutes'
import { productRoutes } from '@/Products/ProductRoutes'
import { articleRoutes } from '@/Articles/ArticleRoutes'
import { exchangeRateRoutes } from '@/ExchangeRate/ExchangeRateRoutes'

Vue.use(VueRouter)

let routes = []

routes = routes.concat(
  {
    path: '/',
    component: Home
  },
  authRoutes,
  productRoutes,
  articleRoutes,
  exchangeRateRoutes,
)

export default new VueRouter({
  mode: 'history',
  routes
})