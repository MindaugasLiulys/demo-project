<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductImage;
use App\Models\Product;
use Faker\Generator as Faker;
use \Illuminate\Support\Facades\File;

$factory->define(ProductImage::class, function (Faker $faker) {
    $filePath = public_path('storage/images/');
    $files = File::files($filePath);

    return [
        'product_id' => factory(Product::class),
        'path' => 'images/' . $files[rand(0, count($files) - 1)]->getFilename(),
    ];
});
