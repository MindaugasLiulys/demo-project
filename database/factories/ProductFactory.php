<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => ucfirst($faker->unique()->word),
        'description' => $faker->text(400),
        'price' => number_format($faker->numberBetween(10, 150), 2),
    ];
});