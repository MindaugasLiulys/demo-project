<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'text' => $faker->realText(rand(30, 400)),
        'username' => $faker->userName,
        'model_id' => null,
        'model_type' => null,
        'created_at' => $faker->dateTimeBetween('-3 years'),
    ];
});