<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use App\Models\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    $title = ucfirst($faker->text(50));
    $paragraphs = [];

    for ($i = 0; $i <= 5; $i ++) {
        $paragraphs[] = '<p>' . $faker->paragraph(20) . '</p>';
    }

    return [
        'title' => $title,
        'slug' => \Illuminate\Support\Str::slug($title),
        'text' => implode('<br>', $paragraphs),
        'author_id' => factory(User::class),
    ];
});