<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Attachment;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Attachment::class, function (Faker $faker) {
    return [
        'filepath' => '/tmp',
        'filename' => Str::random(5) . $faker->fileExtension,
        'filesize' => rand(1000, 10000),
        'group' => null,
        'title' => Str::random(5),
        'model_id' => null,
        'model_type' => null,
    ];
});