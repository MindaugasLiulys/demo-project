<?php

use Illuminate\Database\Seeder;
use App\Models\Variation;

class VariationSeeder extends Seeder
{
    public function run(): void
    {
        $data = $this->getDummyData();

        foreach ($data['colors'] as $color) {
            factory(Variation::class)->create([
                'title' => 'Color',
                'value' => $color,
            ]);
        }

        foreach ($data['sizes'] as $size) {
            factory(Variation::class)->create([
                'title' => 'Size',
                'value' => $size,
            ]);
        }
    }

    private function getDummyData(): array
    {
        $colors = ['Red', 'Green', 'Blue', 'White', 'Black'];
        $sizes = ['XS', 'S', 'M', 'L', 'XL'];

        return [
            'colors' => $colors,
            'sizes' => $sizes,
        ];
    }
}
