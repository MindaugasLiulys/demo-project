<?php

use Illuminate\Database\Seeder;
use App\Models\Article;
use App\Models\User;
use \Illuminate\Support\Facades\Artisan;
use \Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Models\Attachment;
use App\Models\Comment;

class ArticleSeeder extends Seeder
{
    public function run(): void
    {
        $filePath = $this->createAttachmentsDirectory();
        $allFiles = $this->copyDummyImagesToStorage($filePath);

        factory(Article::class, 20)->create([
            'author_id' => User::first()->id,
        ])->each(function ($article) {
            factory(Comment::class, rand(0, 20))->create([
                'model_id' => $article->id,
                'model_type' => Article::class,
            ]);
        });

        $this->createAndAssignAttachments($allFiles);
    }

    private function createAttachmentsDirectory(): string
    {
        // Deletes all attachments from storage
        $filePath = public_path('storage/attachments');
        File::deleteDirectory($filePath);

        Artisan::call('storage:link');

        Storage::makeDirectory('public/attachments');

        return $filePath;
    }

    private function copyDummyImagesToStorage(string $filePath): array
    {
        // Copies dummy images from /public/dummyImages to storage
        $files = File::files(public_path('dummyImages/articles'));
        $allFiles = [];

        foreach ($files as $key => $file) {
            $dummyPath = $files[$key]->getRealPath();
            $file = $files[$key]->getFilename();
            $directory = Str::random(5);
            $fullFilePath = $filePath . '/' . $directory . '/' . $file;

            File::makeDirectory($filePath . '/' . $directory);
            File::copy($dummyPath, $fullFilePath);

            $allFiles[]  = [
                'filepath' => '/public/attachments/' . $directory . '/' . $file,
                'filename' => $file,
                ];
        }

        return $allFiles;
    }

    private function createAndAssignAttachments(array $allFiles): void
    {
        foreach (Article::all()  as $article) {
            $attachmentData = array_merge($allFiles[rand(0, count($allFiles) - 1)], [
                'model_id' => $article->id,
                'model_type' => Article::class,
            ]);

            factory(Attachment::class)->create($attachmentData);
        }
    }
}
