<?php

use Illuminate\Database\Seeder;
use App\Models\ProductImage;
use \Illuminate\Support\Facades\Artisan;
use \Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Category;
use App\Models\Product;
use App\Models\Variation;

class ProductSeeder extends Seeder
{
    public function run(): void
    {
        $filePath = $this->createImagesDirectory();
        $this->copyDummyImagesToStorage($filePath);

        factory(ProductImage::class, 25)->create();

        $this->assignCategoriesToProducts();
        $this->assignVariationsToProducts();
    }

    private function createImagesDirectory(): string
    {
        $filePath = public_path('storage/images');

        if(!File::exists($filePath)){
            Artisan::call('storage:link');

            File::makeDirectory($filePath);
        }

        return $filePath;
    }

    private function copyDummyImagesToStorage(string $filePath): void
    {
        // Deletes dummy images from storage if they exists
        $files = Storage::allFiles('public/images');
        Storage::delete($files);

        // Copies dummy images from /public/dummyImages to storage
        $files = File::files(public_path('dummyImages/products'));

        foreach ($files as $key => $file) {
            $dummyPath = $files[$key]->getRealPath();
            $file = $files[$key]->getFilename();

            File::copy($dummyPath, $filePath . '/' . $file);
        }
    }

    private function assignCategoriesToProducts(): void
    {
        $categories = Category::all();

        Product::all()->each(function (Product $product) use ($categories) {
            $product->categories()->attach(
                $categories->random(rand(1, 3))->pluck('id')->toArray()
            );
        });
    }

    private function assignVariationsToProducts(): void
    {
        $variations = Variation::all();

        Product::all()->each(function (Product $product) use ($variations) {
            $product->variations()->attach(
                $variations->random(rand(2, 10))->pluck('id')->toArray()
            );
        });
    }
}
