<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    public function run(): void
    {
        factory(User::class)->create([
            'email' => 'admin@admin.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
