<?php

namespace App\Managers;

use GuzzleHttp\Client;

class ExchangeRateManager
{
    const DEFAULT_CURRENCY = 'EUR';
    private $apiURL = 'https://api.exchangeratesapi.io/latest';

    public function getAvailableCurrencies(): array
    {
        $response = $this->makeRequest();
        $availableCurrencies = get_object_vars($response->rates);

        return array_keys($availableCurrencies);
    }

    public function getMultipleExchangeRates(array $currencies): array
    {
        $response = $this->makeRequest();
        $ratesArray = get_object_vars($response->rates);
        $rates = [];

        foreach ($currencies as $currency) {
            if (array_key_exists($currency, $ratesArray)) {
                $rates[$currency] = $ratesArray[$currency];
            }
        }

        return $rates;
    }

    public function getExchangeRate(string $currencyTo): float
    {
        if ($currencyTo !== self::DEFAULT_CURRENCY) {
            $symbols = '?symbols=' . $currencyTo;

            $exchangeRate = $this->makeRequest($symbols);

            return $exchangeRate->rates->$currencyTo;
        }

        return 1;
    }

    private function makeRequest(string $symbols = ''): object
    {
        try {
            $client = new Client();
            $response = $client->get($this->apiURL . $symbols);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return json_decode($response->getBody()->getContents());
    }
}
