<?php

namespace App\Managers;

use App\Http\Resources\VariationResource;
use Illuminate\Support\Collection;

class VariationsManager
{
    public function transformVariationsArray(Collection $variations): array
    {
        $transformed = [];
        foreach ($variations as $variation) {
            $transformed[$variation->title][] = new VariationResource($variation);
        }

        return $transformed;
    }
}
