<?php

namespace App\Managers;

use App\Models\Attachment;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AttachmentsManager
{
    public function uploadToStorage(UploadedFile $uploadedFile, Attachment $attachment): void
    {
        Storage::disk('local')->putFileAs(
            $attachment->filepath,
            $uploadedFile,
            $attachment->filename
        );

        $this->updateAttachmentPath($attachment);
    }

    public function createAttachment(UploadedFile $uploadedFile): Attachment
    {
        $data = $this->generateAttachmentData($uploadedFile);

        $attachment = new Attachment();
        $attachment->filepath = $data['filePath'];
        $attachment->filename = $data['fileName'];
        $attachment->filesize = $data['fileSize'];
        $attachment->title = $data['title'];
        $attachment->save();

        return $attachment;
    }

    private function generateAttachmentData(UploadedFile $uploadedFile): array
    {
        $extension = $uploadedFile->getClientOriginalExtension();
        $filePath = '/public/attachments/' . Str::random(5);
        $fileSize = $uploadedFile->getSize();
        $title = $uploadedFile->getClientOriginalName();
        $fileName = $title . '.' . $extension;

        return [
            'extension' => $extension,
            'filePath' => $filePath,
            'fileName' => $fileName,
            'fileSize' => $fileSize,
            'title' => $title,
        ];
    }

    private function updateAttachmentPath(Attachment $attachment): void
    {
        $attachment->update([
            'filepath' => $attachment->filepath . '/' . $attachment->filename,
        ]);
    }
}
