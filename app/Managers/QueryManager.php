<?php

namespace App\Managers;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class QueryManager
{
    private $request;
    private $query;
    private $model;
    private $filters = [];

    function __construct(Request $request, Builder $query, string $model)
    {
        $this->request = $request;
        $this->query = $query;
        $this->model = $model;
    }

    public function get(): LengthAwarePaginator
    {
        $this->search()->filter();

        return $this->query->paginate();
    }

    private function search(): QueryManager
    {
        if ($this->request->has('q')) {
            $this->searchQuery($this->request->query('q'));
        }

        return $this;
    }

    private function filter(): QueryManager
    {
        if (!empty($this->filters)) {
            $values = $this->request->only(array_keys($this->filters));

            if (!$values) return $this;

            $filterValues = [];

            foreach ($values as $key => $value){
                $filterValues = array_merge($filterValues, [
                    Arr::get($this->filters, $key) => $value,
                ]);
            }

            $this->filterRelations($filterValues);
        }

        return $this;
    }

    public function setFilters(array $filters): QueryManager
    {
        $this->filters = $filters;

        return $this;
    }

    private function filterRelations(array $filterData): Builder
    {
        $modifiedQuery = $this->query;

        foreach ($filterData as $key => $filter) {
            $key = Str::snake($key);

            if (empty($filter)) continue;

            $relation = $key;
            $relationKey = $key;

            if (strpos($key, '.') !== false) {
                $exploded = explode('.', $key);
                $relation = $exploded[0];
                $relationKey = $exploded[1];
            }

            if (!method_exists($this->model, $relation)) {
                $modifiedQuery->whereIn($relationKey, $filter);
                continue;
            }

            $modifiedQuery->whereHas($relation, function ($q) use ($relationKey, $filter) {
                $q->whereIn($relationKey, $filter);
            });
        }

        return $modifiedQuery;
    }

    private function searchQuery($keyword): Builder
    {
        $searchableColumns = [
            'title',
        ];

        return $this->query->where(function (Builder $subQuery) use ($searchableColumns, $keyword) {
            foreach ($searchableColumns as $column) {
                $subQuery->orWhere($column, 'LIKE', "%{$keyword}%");
            }
        });
    }
}
