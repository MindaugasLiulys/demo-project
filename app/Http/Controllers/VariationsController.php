<?php

namespace App\Http\Controllers;

use App\Managers\VariationsManager;
use App\Models\Variation;
use Illuminate\Support\Collection;

class VariationsController extends Controller
{
    protected $variationsManager;

    function __construct(VariationsManager $variationsManager)
    {
        $this->variationsManager = $variationsManager;
    }

    public function index(): string
    {
        $variations = Variation::query()->get();
        $transformedResource = $this->variationsManager->transformVariationsArray($variations);

        return json_encode([
            'data' => $transformedResource,
        ]);
    }
}
