<?php

namespace App\Http\Controllers;

use App\Managers\ExchangeRateManager;
use Illuminate\Http\Request;

class ExchangeRateController extends Controller
{
    protected $exchangeRateManager;

    function __construct(ExchangeRateManager $exchangeRateManager)
    {
        $this->exchangeRateManager = $exchangeRateManager;
    }

    public function index(Request $request): array
    {
        $currencies = explode(',', $request->input('currencies'));

        return $this->exchangeRateManager->getMultipleExchangeRates($currencies);
    }

    public function availableCurrencies(): array
    {
        return $this->exchangeRateManager->getAvailableCurrencies();
    }
}
