<?php

namespace App\Http\Controllers;

use App\Http\Resources\ArticleResource;
use App\Managers\QueryManager;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ArticleController extends Controller
{
    public function index(Request $request): AnonymousResourceCollection
    {
        $articles = Article::query()
            ->active()
            ->with('author', 'attachments')
            ->orderByDesc('created_at');

        $query = (new QueryManager($request, $articles, Article::class));

        return ArticleResource::collection($query->get());
    }

    public function show(string $slug): ArticleResource
    {
        $article = Article::query()
            ->where('slug', $slug)
            ->with('author', 'attachments', 'comments')
            ->first();

        return new ArticleResource($article);
    }
}
