<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Managers\QueryManager;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::query()
            ->active()
            ->with('images', 'categories');

        $query = (new QueryManager($request, $products, Product::class))
            ->setFilters([
                'categories' => 'categories.title',
                'variations' => 'variations.value',
            ]);

        return ProductResource::collection($query->get());
    }

    /**
     * @param Product $product
     * @return ProductResource
     */
    public function show(Product $product)
    {
        $product->loadMissing('images', 'categories', 'variations');

        return new ProductResource($product);
    }
}
