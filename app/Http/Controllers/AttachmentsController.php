<?php

namespace App\Http\Controllers;

use App\Http\Resources\AttachmentResource;
use App\Managers\AttachmentsManager;
use App\Models\Attachment;
use Illuminate\Http\Request;

class AttachmentsController extends Controller
{
    protected $attachmentManager;

    function __construct(AttachmentsManager $attachmentManager)
    {
        $this->attachmentManager = $attachmentManager;
    }

    public function uploadFile(Request $request): ?AttachmentResource
    {
        if ($request->hasFile('file')) {
            $uploadedFile = $request->file('file');

            try {
                $attachment = $this->attachmentManager->createAttachment($uploadedFile);
                $this->attachmentManager->uploadToStorage($uploadedFile, $attachment);
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

            return new AttachmentResource($attachment);
        }

        return null;
    }
}
