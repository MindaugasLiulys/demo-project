<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommentResource;
use App\Managers\QueryManager;
use App\Models\Article;
use App\Models\Comment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CommentsController extends Controller
{
    const CLASS_NAMES = [
        'Article' => Article::class,
    ];

    public function getModelComments(Request $request, int $modelId, string $modelType): AnonymousResourceCollection
    {
        $modelType = static::CLASS_NAMES[$modelType];

        $comments = Comment::query()
            ->where('model_id', $modelId)
            ->where('model_type', $modelType)
            ->orderByDesc('created_at');

        $query = (new QueryManager($request, $comments, Comment::class));

        return CommentResource::collection($query->get());
    }

    public function store(Request $request): JsonResponse
    {
        $request['model_type'] = static::CLASS_NAMES[$request->input('model_type')];

        try {
            Comment::query()->create($request->all());
        } catch (\Exception $e) {
            return response()->json(['message' => __($e->getMessage())], 500);
        }

        return response()->json(['message' => __('Comment was posted successfully')]);
    }
}
