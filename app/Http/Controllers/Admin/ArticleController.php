<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArticleRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        /** @var Article $articles */
        $articles = Article::query()
            ->with('comments', 'attachments', 'author')
            ->get();

        return ArticleResource::collection($articles);
    }

    /**
     * @param Article $article
     * @return ArticleResource
     */
    public function show(Article $article)
    {
        $article->loadMissing('attachments', 'comments');

        return new ArticleResource($article);
    }

    /**
     * @param ArticleRequest $request
     * @return ArticleResource
     */
    public function store(ArticleRequest $request)
    {
        /** @var Article $article */
        $article = Article::query()->create($request->validated());
        $article->attach($request->input('attachments'));

        return response()->json(['message' => __('Article was successfully created')]);
    }

    /**
     * @param ArticleRequest $request
     * @param Article $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ArticleRequest $request, Article $article)
    {
        /** @var Article $article */
        $article = tap($article)->update($request->validated());
        $article->attach($request->input('attachments'));

        return response()->json(['message' => __('Article was successfully updated')]);
    }

    /**
     * @param Article $article
     * @return bool|null
     */
    public function destroy(Article $article)
    {
        $article->delete();

        return response()->json(['message' => __('Article was successfully deleted')]);
    }
}