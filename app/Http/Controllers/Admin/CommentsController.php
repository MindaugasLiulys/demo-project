<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;

class CommentsController extends Controller
{
    /**
     * @param Comment $comment
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Comment $comment)
    {
        try {
            $comment->delete();
        } catch (\Exception $e) {
            return response()->json(['message' => __($e->getMessage())], 500);
        }

        return response()->json(['message' => __('Comment was successfully deleted')]);
    }
}