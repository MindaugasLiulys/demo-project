<?php

namespace App\Http\Resources;

use App\Models\Comment;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var Comment $comment */
        $comment = $this->resource;

        return [
            'id' => $comment->id,
            'text' => $comment->text,
            'username' => $comment->username,
            'model' => $comment->model(),
            'createdAt' => $comment->created_at->toDateString(),
        ];
    }
}
