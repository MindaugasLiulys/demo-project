<?php

namespace App\Http\Resources;

use App\Models\Attachment;
use Illuminate\Http\Resources\Json\JsonResource;

class AttachmentResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var Attachment $attachment */
        $attachment = $this->resource;

        return [
            'id' => $attachment->id,
            'filepath' => $attachment->filepath,
            'filename' => $attachment->filename,
            'filesize' => $attachment->filesize,
            'group' => $attachment->group,
            'title' => $attachment->title,
            'previewPath' => $attachment->previewPath,
        ];
    }
}
