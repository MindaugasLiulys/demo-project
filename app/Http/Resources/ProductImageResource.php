<?php

namespace App\Http\Resources;

use App\Models\ProductImage;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductImageResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var ProductImage $productImage */
        $productImage = $this->resource;

        return [
            'id' => $productImage->id,
            'path' => $productImage->path,
            'product' => new ProductResource($this->whenLoaded('product')),
        ];
    }
}
