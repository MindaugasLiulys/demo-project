<?php

namespace App\Http\Resources;

use App\Models\Variation;
use Illuminate\Http\Resources\Json\JsonResource;

class VariationResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var Variation $variation */
        $variation = $this->resource;

        return [
            'id' => $variation->id,
            'title' => $variation->title,
            'value' => $variation->value,
            'products' => ProductResource::collection($this->whenLoaded('products')),
            'productsCount' => $variation->products->count(),
        ];
    }
}
