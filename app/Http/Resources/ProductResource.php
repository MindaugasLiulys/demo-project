<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var Product $product */
        $product = $this->resource;

        return [
            'id' => $product->id,
            'title' => $product->title,
            'description' => $product->description,
            'price' => $product->price,
            'status' => $product->status,
            'images' => ProductImageResource::collection($this->whenLoaded('images')),
            'categories' => CategoryResource::collection($this->whenLoaded('categories')),
            'variations' => $this->transformVariationsArray($product),
        ];
    }

    private function transformVariationsArray(Product $product): array
    {
        $variations = [];

        if ($this->relationLoaded('variations')) {
            foreach ($product->variations as $variation) {
                $variations[$variation->title][] = new VariationResource($variation);
            }
        }

        return $variations;
    }
}
