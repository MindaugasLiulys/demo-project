<?php

namespace App\Http\Resources;

use App\Models\Article;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var Article $article */
        $article = $this->resource;

        return [
            'id' => $article->id,
            'title' => $article->title,
            'slug' => $article->slug,
            'text' => $article->text,
            'author' => new UserResource($this->whenLoaded('author')),
            'status' => $article->status,
            'statusText' => Article::STATUS_TEXT[$article->status],
            'attachments' => AttachmentResource::collection($this->whenLoaded('attachments')),
            'comments' => CommentResource::collection($this->whenLoaded('comments')),
            'modelType' => 'Article',
            'createdAt' => $article->created_at->toDateString(),
        ];
    }
}
