<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TransformsRequest;
use Illuminate\Support\Str;

class SnakeCaseInputs extends TransformsRequest
{
    /**
     * The attributes that should not be transformed.
     *
     * @var array
     */
    protected $except = [];

    /**
     * Clean the data in the given array.
     *
     * @param  array $data
     * @param string $keyPrefix
     * @return array
     */
    protected function cleanArray(array $data, $keyPrefix = '')
    {
        return collect($data)->mapWithKeys(function ($value, $key) {
            return [$this->cleanKey($key) => $this->cleanValue($key, $value)];
        })->all();
    }

    /**
     * Convert request keys to snake_case
     *
     * @param $key
     * @return string
     */
    protected function cleanKey($key)
    {
        if (in_array($key, $this->except)) {
            return $key;
        }

        // We need this check here, because we don't want to replace anything if the key is only a digit.
        return preg_match('/([a-z]+)/', $key) ? preg_replace('/(\d+)/', '_$1', Str::snake($key)) : Str::snake($key);
    }
}
