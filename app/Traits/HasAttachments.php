<?php

namespace App\Traits;

use App\Models\Attachment;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;

trait HasAttachments
{
    public function attachments(): MorphMany
    {
        return $this->morphMany(Attachment::class, 'model');
    }

    public function attachmentsGroup($group): Collection
    {
        return $this->attachments()->where('group', $group)->get();
    }

    public function attach(array $attachments): void
    {
//        Todo: Check for duplicates
        foreach ($attachments as $attachment) {
            /** @var Attachment $attachment */
            $attachment = Attachment::query()->find($attachment['id']);

            $this->attachments()->save($attachment);
        }
    }
}
