<?php

namespace App\Traits;

use App\Models\Comment;

trait HasComments
{
    /**
     * @return mixed
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'model')->orderByDesc('created_at');
    }

    /**
     * @param Comment $comment
     * @return null
     */
    public function attachComment(Comment $comment)
    {
        $this->comments()->save($comment);

        return null;
    }
}