<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int id
 * @property string title
 * @property string value
 * @property string notes
 *
 * @property-read Product products
 */
class Variation extends Model
{
    protected $fillable = [
        'title',
        'value',
        'notes',
    ];

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_variations')->withPivot('price');
    }
}
