<?php

namespace App\Models;

use App\Traits\HasAttachments;
use App\Traits\HasComments;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

/**
 * @property int id
 * @property string title
 * @property string slug
 * @property string text
 * @property int author_id
 * @property int status
 * @property Carbon created_at
 *
 * @property-read User author
 */
class Article extends Model
{
    use HasAttachments, HasComments;

    const STATUS_DEACTIVATED = 0;
    const STATUS_ACTIVE = 1;

    const STATUS_TEXT = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_DEACTIVATED => 'Not active',
    ];

    protected $fillable = [
        'title',
        'slug',
        'text',
        'author_id',
        'status',
    ];

    protected $perPage = 9;

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            if (! $model->slug) {
                $model->slug = Str::slug($model->title);
            }

            if (auth()->check()) {
                $model->author_id = auth()->user()->id;
            }
        });
    }

    public function scopeActive($query): Builder
    {
        return $query->where('status', $this::STATUS_ACTIVE);
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, 'author_id');
    }
}
