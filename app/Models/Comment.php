<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @property int id
 * @property string text
 * @property string username
 * @property int model_id
 * @property string model_type
 * @property Carbon created_at
 */
class Comment extends Model
{
    protected $fillable = [
        'text',
        'username',
        'model_id',
        'model_type',
    ];

    public function model(): MorphTo
    {
        return $this->morphTo();
    }
}
