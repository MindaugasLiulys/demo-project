<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string filepath
 * @property string filename
 * @property int filesize
 * @property string group
 * @property string title
 * @property int model_id
 * @property string model_type
 * @property string previewPath
 */
class Attachment extends Model
{
    protected $fillable = [
        'filepath',
        'filename',
        'filesize',
        'group',
        'title',
        'model_id',
        'model_type',
    ];

    public function getPreviewPathAttribute(): string
    {
        return str_replace('public', 'storage', $this->filepath);
    }
}
