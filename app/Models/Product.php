<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int id
 * @property string title
 * @property string description
 * @property int price
 * @property int status
 *
 * @property-read ProductImage images
 * @property-read Category categories
 * @property-read Variation variations
 */
class Product extends Model
{
    const STATUS_DEACTIVATED = 0;
    const STATUS_ACTIVE = 1;

    protected $fillable = [
        'title',
        'description',
        'price',
        'status',
    ];

    protected $perPage = 9;

    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', $this::STATUS_ACTIVE);
    }

    public function images(): HasMany
    {
        return $this->hasMany(ProductImage::class);
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'product_categories');
    }

    public function variations(): BelongsToMany
    {
        return $this->belongsToMany(Variation::class, 'product_variations')->withPivot('price');
    }
}
