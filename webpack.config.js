const path = require('path')
// const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './resources/js/components'),
      '*': path.resolve(__dirname, './resources/js')
    }
  },
  // plugins: [
  //   new VueLoaderPlugin()
  // ],
  module: {
    rules: [
      {
        test: /\.(mjs|js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',


        options: {
          presets: [
            '@babel/preset-env',
            {
              plugins: [
                '@babel/plugin-proposal-class-properties'
              ]
            }
          ]
        },


      }
    ],
  }
}
