<?php

Route::post('/auth/register', 'Auth\RegisterController@register')->name('register');
Route::post('/auth/login', 'Auth\LoginController@login')->name('login');

// Refresh the JWT Token
Route::get('/auth/refresh', 'Auth\AuthController@refresh')->name('refresh');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/auth/user', 'Auth\AuthController@user')->name('auth.user');
    Route::post('/logout', 'Auth\LoginController@logout');

    Route::group(['prefix' => 'admin'], function () {
        Route::apiResource('/articles', 'Admin\ArticleController', ['except' => 'update']);
        Route::post('/articles/update/{article}', 'Admin\ArticleController@update');
        Route::delete('/comments/{comment}', 'Admin\CommentsController@destroy');

        Route::post('/attachments/upload', 'AttachmentsController@uploadFile');
    });
});

Route::apiResource('/products', 'ProductController', ['only' => ['index', 'show']]);
Route::apiResource('/articles', 'ArticleController', ['only' => ['index', 'show']]);
Route::apiResource('/categories', 'CategoryController', ['only' => ['index']]);
Route::apiResource('/variations', 'VariationsController', ['only' => ['index']]);
Route::apiResource('/comments', 'CommentsController', ['only' => ['index', 'store', 'destroy']]);
Route::get('/comments/get-model-comments/{modelId}/{modelType}', 'CommentsController@getModelComments');

Route::get('/exchange-rate', 'ExchangeRateController@index');
Route::get('/exchange-rate/available-currencies', 'ExchangeRateController@availableCurrencies');