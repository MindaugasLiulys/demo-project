const mix = require('laravel-mix');

mix.webpackConfig(require('./webpack.config.js'))

mix.js('resources/js/app.js', 'public/js')
  .sass('resources/sass/app.scss', 'public/css')
  .version();
